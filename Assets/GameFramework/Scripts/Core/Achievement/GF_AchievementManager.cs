﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

namespace GameFramework
{
    public class GF_AchievementManager
    {
        public GF_Achievement[] Achievements;
        public AudioClip EarnedSound;
        public GUIStyle GuiStyleAchievementEarned;
        //public GUIStyle GUIStyleAchievementNotEarned;

        public GUIStyle GuiStyleAchievementNotEarned2;
        internal bool ShowAcmWindow = false;

        private int _currentRewardPoints = 0;
        private int _potentialRewardPoints = 0;
        private Vector2 _achievementScrollviewLocation = Vector2.zero;

        void Start()
        {
            ValidateAchievements();
            UpdateRewardPointTotals();
        }

        // Make sure the setup assumptions we have are met.
        private void ValidateAchievements()
        {
            var usedNames = new ArrayList();
            foreach (GF_Achievement achievement in Achievements)
            {
                if (achievement.RewardPoints < 0)
                {
                    Debug.LogError("AchievementManager::ValidateAchievements() - GF_Achievement with negative RewardPoints! " + achievement.Name + " gives " + achievement.RewardPoints + " points!");
                }

                if (usedNames.Contains(achievement.Name))
                {
                    Debug.LogError("AchievementManager::ValidateAchievements() - Duplicate achievement names! " + achievement.Name + " found more than once!");
                }
                usedNames.Add(achievement.Name);
            }
        }

        private GF_Achievement GetAchievementByName(string achievementName)
        {
            return Achievements.FirstOrDefault(achievement => achievement.Name == achievementName);
        }

        private void UpdateRewardPointTotals()
        {
            _currentRewardPoints = 0;
            _potentialRewardPoints = 0;

            foreach (GF_Achievement achievement in Achievements)
            {
                if (achievement.Earned)
                {
                    _currentRewardPoints += achievement.RewardPoints;
                }

                _potentialRewardPoints += achievement.RewardPoints;
            }
        }

        private void AchievementEarned()
        {
            UpdateRewardPointTotals();
            AudioSource.PlayClipAtPoint(EarnedSound, Camera.main.transform.position);
        }

        internal void AddProgressToAchievement(string achievementName, float progressAmount)
        {
            GF_Achievement achievement = GetAchievementByName(achievementName);
            if (achievement == null)
            {
                Debug.LogWarning("AchievementManager::AddProgressToAchievement() - Trying to add progress to an achievemnet that doesn't exist: " + achievementName);
                return;
            }

            if (achievement.AddProgress(progressAmount))
            {
                AchievementEarned();
            }
        }

        public void SetProgressToAchievement(string achievementName, float newProgress)
        {
            GF_Achievement achievement = GetAchievementByName(achievementName);
            if (achievement == null)
            {
                Debug.LogWarning("AchievementManager::SetProgressToAchievement() - Trying to add progress to an achievemnet that doesn't exist: " + achievementName);
                return;
            }

            if (achievement.SetProgress(newProgress))
            {
                AchievementEarned();
            }
        }

        void OnGUI()
        {

            if (ShowAcmWindow)
            {

                float yValue = 5.0f;
                float achievementGUIWidth = 500.0f;

                GUI.Label(new Rect(200.0f, 5.0f, 200.0f, 25.0f), "-- Achievements --");

                // CharacterSetup _achievement scrollview, and then fill it with each achievement in our list.

                _achievementScrollviewLocation = GUI.BeginScrollView(new Rect(0.0f, 25.0f, achievementGUIWidth + 25.0f, 400.0f), _achievementScrollviewLocation,
                                                                    new Rect(0.0f, 0.0f, achievementGUIWidth, Achievements.Count() * 80.0f));

                foreach (GF_Achievement achievement in Achievements)
                {
                    Rect position = new Rect(5.0f, yValue, achievementGUIWidth, 75.0f);
                    achievement.DrawGUI(position, GuiStyleAchievementEarned, GuiStyleAchievementNotEarned2);
                    yValue += 80.0f;
                }

                GUI.EndScrollView();

                GUI.Label(new Rect(10.0f, 440.0f, 200.0f, 25.0f), "Reward Points: [" + _currentRewardPoints + " out of " + _potentialRewardPoints + "]");

            }

        }
        internal void ToggleAchievementWindow()
        {
            ShowAcmWindow = !ShowAcmWindow;
        }
    }
}
