namespace GameFramework
{

    public enum StatModifierType
    {
        Additive, OnlyOnce
    }

    public enum StatClampType
    {
        RawFloat,
        Round,
        Floor
    }

    public enum Team
    {
        Neutral, // neutral to all
        Aggressive, // aggressive to all
        Red, Blue, Green //...
    }

    public enum Stat
    {
        Might, Strength, Spirit
    }

    public enum GameMode { Singleplayer, Multiplayer }

    public enum UnitDifficultyColors { Grey, Green, Yellow, Orange, Red, Skull }

    public enum WeaponType 
    {
        Bombthrower, Gun, Missilelauncher
    }
    public enum GameOption
    {
        Option1, Option2
    }

    public enum GameDifficultyLevel
    {
        Easy, Normal, Advanced, Heavy, Hardcore, Hell
    }

    public enum GamePlayMode
    {
        SinglePlayer,
        MultiPlayer

    }

    public enum VersusModes
    {
        Pve,
        Pvp, // needs Multiplayer
        Survival // needs Multiplayer
    }

    public enum TeamSize
    {
        TeamSize1,
        TeamSize2,
        TeamSize3,
        TeamSize4,
        TeamSize5,
        TeamSize6,
        TeamSize8,
        TeamSize10,
        TeamSize15,
        TeamSize20,
        TeamSize25,
        TeamSize40
    }
}