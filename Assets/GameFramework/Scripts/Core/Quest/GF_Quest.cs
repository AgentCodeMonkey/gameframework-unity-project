﻿using System.Collections.Generic;
using System;

/// <summary>
/// Solo, Group, Dungeon, Raid, Heroic, Pvp, Daily, Repeatable, Seasonal
/// </summary>
public enum QuestTypes { Solo, Group, Dungeon, Raid, Heroic, Pvp, Daily, Repeatable, Seasonal }
///// <summary>
/////  Nowhere, Zone1, Zone2
///// </summary>
//public enum QuestZones { Nowhere, Zone1, Zone2 }
/// <summary>
/// Collect, Kill, Escort, Deliver, Explore, FindAndSpeakToNPC, Profession
/// </summary>
public enum QuestVariations { Collect, Kill, Escort, Deliver, Explore, FindAndSpeakToNPC, Profession }
/// <summary>
/// Available, Unavailable, Accepted, Complete, Done
/// </summary>
public enum QuestStates { Available, Unavailable, Accepted, Completed, Done }
/// <summary>
///  HasNoQuest, HasQuest
/// </summary>
public enum QuestDialogStates { HasNoQuest, HasQuest }

public class GF_Quest : ICloneable
{
    public GF_Quest() { }

    public int ID = 0;
    public int Level = 1; /// das level der mission zb 10
    public int RequiredLevel = 1; /// das level um die mission an zu nehmen, zb 8, wenn Level 10 ist
    //public FactionSide FactionSide = FactionSide.Alliance;
    public QuestTypes Type = QuestTypes.Solo;
    public string Zone = "Zone1";
    public QuestVariations Variation = QuestVariations.Collect;
    public int RewardExperience = 0;
    public int RewardMoney = 0;
    public int RewardRepuation = 0;
    public int FactionRepuation = 0;
    public int NextMissionID = 0;
    public int RequiredMissionID = 0;
    public int QuestChainID = 0;
    public string Name = "<name goes here>";
    public string CompletionText = "Greetings, player...I see you have returned from battle, but i still have NO Completion TEXT for you! ";
    public string Description = "<description goes here>";
    public string Summary = "<summary goes here>";
    public string GiverNameStart = "MissionGiverName_Start";
    public string GiverNameEnd = "MissionGiverName_End";
    public List<string> RewardItems;
    public QuestStates State = QuestStates.Available;
    public DateTime StartTime = DateTime.MinValue;

    public string Requirement1Name = "<Requirement1Name goes here>";
    public string Requirement2Name = "<Requirement2Name goes here>";
    public float Requirement1Count = 0;
    public float Requirement2Count = 0;

    public List<string> Test;

    public float Progress1 = 0;
    public float Progress2 = 0;

    public bool IsAchievement = false;

    public object Clone()
    {
        var clone = MemberwiseClone() as GF_Quest;
        return clone ?? null;
    }
}