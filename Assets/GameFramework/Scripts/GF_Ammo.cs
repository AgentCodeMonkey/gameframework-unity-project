using GameFramework;
using UnityEngine;
using System.Collections;

[System.Serializable]
public class AmmoMagazine
{
    public string AmmoName;
    public GF_Ammo GfAmmoType;
    public int Ammo;
    public int AmmoInMagazines;
    public int AmmoMagazines;
    public int AmmoCapacity;
    public float ReloadTime;
    public bool IsReady = true;
    public bool IsReloading = false;


    public void Init()
    {
        AmmoInMagazines = Mathf.Clamp(AmmoInMagazines, 0, AmmoCapacity);
        Ammo = AmmoMagazines*AmmoCapacity;
        AmmoName = GfAmmoType.AmmoName;
    }

    public int GetMagazines()
    {
        float ammoFloat = Ammo / AmmoCapacity;
        var ammoInt = Mathf.FloorToInt(ammoFloat);
        if (ammoFloat < ammoInt)
            return ammoInt + 1;
        return ammoInt;
    }

    #region ReloadWeapons

    public IEnumerator ReloadWeapons(bool b)
    {
        IsReloading = true;
        Debug.Log("Reload");
        if (AmmoMagazines > 0)
        {
            yield return new WaitForSeconds(ReloadTime);
            if (b)
                MagazineReloader();
            else
                SetMagazines();
        }
        else 
            yield return new WaitForSeconds(0);
        IsReloading = false;
    }

    #endregion ReloadWeapons

    #region MagazineReloader

    public void MagazineReloader()
    {
        if (AmmoMagazines > 0)
        {
            AmmoMagazines--;
        }
        SetMagazines();
    }

    public void SetMagazines()
    {
        Ammo = AmmoCapacity * AmmoMagazines;
        AmmoInMagazines = AmmoCapacity > Ammo ? Ammo : AmmoCapacity;
        IsReady = AmmoInMagazines > 0;
    }

    #endregion MagazineReloader

    #region AddAmmo

    internal void AddAmmo(int amount)
    {
        Ammo += amount;
        SetAmmo(AmmoMagazines * AmmoCapacity);
    }

    internal void AddMagazines(int amount)
    {
        AmmoMagazines += amount;
        SetAmmo(AmmoMagazines * AmmoCapacity);
    }

    #endregion AddAmmo

    public void SetAmmo(int ammo)
    {
        Ammo = ammo;
        AmmoMagazines = GetMagazines();
    }

    public void SubAmmo(int ammo)
    {
        AmmoInMagazines -= ammo;
        Ammo -= ammo;
    }

    public void SubAmmo()
    {
        AmmoInMagazines--;
        Ammo--;
    }
}

public class GF_Ammo : GF_Unit
{
    public GF_Unit Owner;
    public string AmmoName = "AmmoName";

    public void SetOwner(GF_Unit o)
    {
        Owner = o;
    }

    // Use this for initialization
	void Awake ()
	{
	    Destroy(gameObject, 5f);
	}
	
	// Update is called once per frame
	public override void Update ()
	{
	    transform.Translate(Vector3.forward * Time.deltaTime * 30);
	}

    void OnTriggerEnter(Collider c)
    {
        GF_Unit u;
        if (c.CompareTag("Player"))
        {
            Debug.Log("!!!");
            u = c.gameObject.GetComponent<GF_Unit>();
            if (u.UnitAttribute.Team == UnitAttribute.Team) return;
            u.SubHealth(Owner, GF_BattleSystem.GetDamage(Owner, u));
            //Debug.Log(u.UnitAttribute.GF_Health);
            Destroy(gameObject);
        }


        if (c.CompareTag("Building"))
        {
            Debug.Log("!!!");
            if (!c.gameObject.GetComponent<GF_Building>())
            {
                Debug.Log("target isn�t a unit");
                return;
            }
            u = c.gameObject.GetComponent<GF_Building>();
            if (u.UnitAttribute.Team == UnitAttribute.Team) return;
            u.SubHealth(Owner, GF_BattleSystem.GetDamage(Owner, u));
            //Debug.Log(u.UnitAttribute.GF_Health);
            Destroy(gameObject);
        }
        
    }
}
