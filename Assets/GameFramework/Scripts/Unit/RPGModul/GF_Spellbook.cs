using System.Linq;
using GameFramework;
using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpellInfo
{
    public string SpellName;
    public StatModifier[] StatModifiers;
}

public class GF_Spellbook : MonoBehaviour
{
    public GF_Unit unit;

    public SpellInfo[] Spells;

    void Start()
    {
        unit = GetComponent<GF_Unit>();
    }

    void Update()
    {
    }

    void OnGUI()
    {
        GUI.Label(new Rect(365.0f, 150.0f, 300.0f, 30.0f), "Spells:");

        float yValue = 125.0f;
        foreach (SpellInfo spellInfo in Spells)
        {
            
            if (GUI.Button(new Rect(265.0f, yValue, 200.0f, 25.0f), spellInfo.SpellName))
            {
                foreach (StatModifier statModifier in spellInfo.StatModifiers)
                {
                    switch (statModifier.ModifierType)
                    {
                        case StatModifierType.Additive:

                            unit.UnitAttribute.AddModifier((StatModifier) statModifier.Clone());
                            break;
                        case StatModifierType.OnlyOnce:
                            if (unit.UnitAttribute.modifiers.Any(mod => mod.StatName == statModifier.StatName))
                            {
                                Debug.Log("Buff already added to Buff list!");
                                return;
                            }
                            unit.UnitAttribute.AddModifier((StatModifier) statModifier.Clone());
                            break;
                    }
                }
            }
            yValue += 30.0f;
        }
    }
}
