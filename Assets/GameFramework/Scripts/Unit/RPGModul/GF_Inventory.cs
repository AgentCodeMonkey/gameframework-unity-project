using UnityEngine;
using System.Collections;

[System.Serializable]
public class ItemInfo
{
    public string ItemName;
    public StatModifier[] StatModifiers;
}

public class GF_Inventory : MonoBehaviour
{
    public GF_Unit unit;

    public ItemInfo[] Weapons;
    private int currentWeapon;
    private int previousCurrentWeapon;

    void Start()
    {
        currentWeapon = 0;
        previousCurrentWeapon = 0;
        unit = GetComponent<GF_Unit>();
    }

    void Update()
    {
    }

    void OnGUI()
    {
        GUI.Label(new Rect(265.0f, 25.0f, 300.0f, 30.0f), "Weapon:");

        string[] weaponStrings = new string[Weapons.GetLength(0) + 1];
        weaponStrings[0] = "None";
        int stringIndex = 1;
        foreach (ItemInfo itemInfo in Weapons)
        {
            weaponStrings[stringIndex] = itemInfo.ItemName;
            stringIndex++;
        }

        currentWeapon = GUI.Toolbar(new Rect(250.0f, 50.0f, 300.0f, 30.0f), currentWeapon, weaponStrings);

        if (GUI.changed && currentWeapon != previousCurrentWeapon)
        {
            UnequipWeapon(previousCurrentWeapon);
            EquipWeapon(currentWeapon);
            previousCurrentWeapon = currentWeapon;
        }
    }

    void EquipWeapon(int weaponIndex)
    {
        if (weaponIndex == 0)
        {
            return;
        }

        ItemInfo itemInfo = Weapons[weaponIndex - 1];
        foreach (StatModifier statModifier in itemInfo.StatModifiers)
        {
            unit.UnitAttribute.AddModifier((StatModifier)statModifier.Clone());
        }
    }

    void UnequipWeapon(int weaponIndex)
    {
        if (weaponIndex == 0)
        {
            return;
        }

        ItemInfo itemInfo = Weapons[weaponIndex - 1];
        foreach (StatModifier statModifier in itemInfo.StatModifiers)
        {
            unit.UnitAttribute.RemoveModifier((StatModifier)statModifier.Clone());
        }
    }
}
