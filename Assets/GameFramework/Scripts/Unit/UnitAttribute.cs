using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameFramework;
using System.Linq;

[System.Serializable]
public class StatModifier : ICloneable
{
    public string StatName;
    public StatModifierType ModifierType;
    public float Amount;
    public float Duration;

    public StatModifier(string statName, StatModifierType statModifierType, float amount, float duration)
    {
        StatName = statName;
        ModifierType = statModifierType;
        Amount = amount;
        Duration = duration;
    }

    public object Clone()
    {
        return MemberwiseClone();
    }

    public override bool Equals(object obj)
    {
        StatModifier other = obj as StatModifier;
        if (other != null && other.StatName == StatName && other.ModifierType == ModifierType && other.Amount == Amount && other.Duration == Duration)
        {
            return true;
        }

        return false;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}

[System.Serializable]
public class UnitAttribute
{
    [System.Serializable]
    public class StatEntry
    {
        public string Name;
        public float StartingValue;
        public float MinValue;
        public float CurValue;
        public float MaxValue;
        public StatClampType ClampType;
    }

    public List<StatEntry> Stats = new List<StatEntry>();
    public List<StatModifier> modifiers = new List<StatModifier>();

    public string Name = "test";
    public Team Team;
    //public WeaponType WeaponType;
    public int Health;
    public int MaxHealth;
    public int Ammo = 100;
    public int Armor = 60;
    public int Attackpower = 10;

    public bool CanMove = true;
    public bool IsAlive = true;
    public bool CanJump = true;

    public int ExperiencePoints = 0;
    public int Level = 1;


    public float RunSpeed = 4F;
    public float WalkSpeed = 2F;
    public float BackSpeed = 2F;

    public float Speed = 1.0F;
    public float JumpSpeed = 8.0F;
    public float GravityMultiplier = 1F;

    public void Start()
    {
        ValidateAllStats();
    }

    public void Update()
    {
        if (modifiers.Count <= 0) return;
        for (int i = modifiers.Count - 1; i >= 0; i--)
        {
            StatModifier statMod = (StatModifier)modifiers[i];
            if (statMod.Duration > 0.0f)
            {
                statMod.Duration -= Time.deltaTime;
                if (statMod.Duration < 0.0f)
                {
                    modifiers.RemoveAt(i);
                    i--;
                }
            }
        }
        foreach (StatEntry statEntry in Stats)
        {
            statEntry.CurValue = GetStatValue(statEntry.Name);
        }
    }

    public void OnGUI()
    {
        GUI.Label(new Rect(5.0f, 25.0f, 250.0f, 25.0f), "--- Stats ---");
        float yValue = 50.0f;

        foreach (StatEntry statEntry in Stats)
        {

            GUI.Label(new Rect(5.0f, yValue, 250.0f, 25.0f), statEntry.Name + ": " + statEntry.CurValue);
            yValue += 25.0f;
        }
        yValue += 25.0f;
        GUI.Label(new Rect(5.0f, yValue, 250.0f, 25.0f), "--- Active Modifiers ---");

        yValue += 25.0f;
        foreach (StatModifier statModifier in modifiers)
        {
            string modifierAmount;
            if (statModifier.Amount >= 0.0f)
            {
                modifierAmount = "+" + statModifier.Amount;
            }
            else
            {
                modifierAmount = statModifier.Amount.ToString();
            }

            if (statModifier.Duration > 0.0f)
            {
                GUI.Label(new Rect(5.0f, yValue, 250.0f, 25.0f), statModifier.StatName + ": " + modifierAmount + " (" + statModifier.Duration + ")");
            }
            else
            {
                GUI.Label(new Rect(5.0f, yValue, 250.0f, 25.0f), statModifier.StatName + ": " + modifierAmount);
            }

            yValue += 25.0f;
        }
    }

    private StatEntry GetStatEntry(string statName)
    {
        return Stats.FirstOrDefault(statInfo => statInfo.Name == statName);
    }

    public float GetStatValue(string statName)
    {
        StatEntry statEntry = GetStatEntry(statName);

        if (statEntry == null)
        {
            Debug.LogWarning("StatController::GetStatValue() - Tried to get a stat value for a stat that doesn't exist: " + statName);
            return -1.0f;
        }

        float returnValue = statEntry.StartingValue;
        returnValue = ApplyModifiersToStatValue(statName, returnValue);
        returnValue = Mathf.Clamp(returnValue, statEntry.MinValue, statEntry.MaxValue);
        returnValue = ClampStatValue(statEntry.ClampType, returnValue);
        statEntry.CurValue = returnValue;
        return returnValue;
    }

    private float ClampStatValue(StatClampType clampType, float baseValue)
    {
        switch (clampType)
        {
            case StatClampType.RawFloat:
                return baseValue;
            case StatClampType.Floor:
                return Mathf.Floor(baseValue);
            case StatClampType.Round:
                return Mathf.Round(baseValue);
        }

        return baseValue;
    }

    private float ApplyModifiersToStatValue(string statName, float baseValue)
    {
        foreach (StatModifier statModifier in modifiers.Where(statModifier => statModifier.StatName == statName))
        {
            
            baseValue += statModifier.Amount;
            
            //else
            //{
            //    Debug.LogWarning("StatController::ApplyModifiersToStatValue() - Unsupported stat modifier type!");
            //}
        }

        return baseValue;
    }

    public void RemoveModifier(StatModifier statModifier)
    {
        modifiers.Remove(statModifier);
    }

    public void AddModifier(StatModifier statModifier)
    {
        bool foundStat = Stats.Any(statEntry => statEntry.Name == statModifier.StatName);
        if (foundStat == false)
        {
            Debug.LogError("StatController::AddModifier() - Trying to add a modifier for a stat that doesn't exist! Stat: " + statModifier.StatName);
            return;
        }

        modifiers.Add(statModifier);
    }

    private void ValidateAllStats()
    {
        ArrayList usedNames = new ArrayList();
        foreach (StatEntry statEntry in Stats)
        {
            if (statEntry.Name.Length == 0)
            {
                Debug.LogError("StatController::ValidateAllStats() - Stat shouldn't have a name with a length of 0!");
            }

            if (usedNames.Contains(statEntry.Name))
            {
                Debug.LogError("StatController::ValidateAllStats() - Stat [" + statEntry.Name + "] is in the Stats list more than once!");
            }

            usedNames.Add(statEntry.Name);

            if (statEntry.MinValue > statEntry.MaxValue)
            {
                Debug.LogError("StatController::ValidateAllStats() - Stat [" + statEntry.Name + "] has a MinValue > MaxValue!");
            }

            if (statEntry.StartingValue < statEntry.MinValue || statEntry.StartingValue > statEntry.MaxValue)
            {
                Debug.LogError("StatController::ValidateAllStats() - Stat [" + statEntry.Name + "] starting value of " + statEntry.StartingValue + " is not within the MinValue/MaxValue range of " + statEntry.MinValue + " -> " + statEntry.MaxValue);
            }

            statEntry.CurValue = statEntry.StartingValue;
        }
    }


    public void SetExperiencePointsCap(int exp)
    {
        ExperiencePoints = exp;
    }

    //public Dictionary<Stat, float> Stats = new Dictionary<Stat, float>();

}
