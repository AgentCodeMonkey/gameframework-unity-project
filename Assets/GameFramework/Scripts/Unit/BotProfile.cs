using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameFrameWork
{
    public class BotProfile
    {
        public static List<string> BotNames = new List<string>()
	    {
            "HI THERE",
            "TestBuddy",
            "Cuby McCube",
            "Bot",
            "BAMBAM",
            "Flint",
            "Rockefeller",
            "ROFL"
	    };

        public static string RandomBotName()
        {
            return BotNames[Random.Range(0, BotNames.Count)];
        }
    }


    
}