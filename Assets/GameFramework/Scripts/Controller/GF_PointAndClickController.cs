﻿using UnityEngine;
using System.Collections;

public class GF_PointAndClickController : GF_InputManager
{
    //Vector3 _dir = Vector3.zero;
    private Transform _myTransform;				// this transform
    private Vector3 _destinationPosition;		// The destination Point
    private float _destinationDistance;			// The distance between myTransform and destinationPosition
    public float MoveSpeed;						// The Speed the character will move
    public float TurnSpeed;
    //private Vector3 _targetPoint = Vector3.zero;

    void Start()
    {
        _myTransform = transform;							// sets myTransform to this GameObject.transform
        _destinationPosition = _myTransform.position;		// prevents myTransform reset
    }

    void Update()
    {
        // keep track of the distance between this gameObject and destinationPosition
        _destinationDistance = (_destinationPosition - _myTransform.position).sqrMagnitude;

        if (_destinationDistance <= 1f)
        {
            _destinationPosition = _myTransform.position;
        }
        else if (_destinationDistance > 1f)
        {
            _myTransform.position = Vector3.MoveTowards(_myTransform.position, _destinationPosition, MoveSpeed * Time.deltaTime);
            _myTransform.rotation = GetDestination();

            //variante 2 - nicht komplett
            //_dir = (myTransform.position - destinationPosition).normalized;
            //var myDirection = Vector3.Dot(_dir, transform.forward);
            //myTransform.rotation = GetDestination();

            // variante 3
            //Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            //myTransform.rotation = targetRotation;
        }


        // Moves the Player if the Left Mouse Button was clicked
        if (Input.GetMouseButtonDown(1) && GUIUtility.hotControl == 0)
        {
            Plane playerPlane = new Plane(Vector3.up, _myTransform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float hitdist = 0.0f;
            if (playerPlane.Raycast(ray, out hitdist))
            {
                //_targetPoint = ray.GetPoint(hitdist);
                _destinationPosition = ray.GetPoint(hitdist);
            }
        }

        // Moves the player if the mouse button is hold down
        else if (Input.GetMouseButton(1) && GUIUtility.hotControl == 0)
        {
            Plane playerPlane = new Plane(Vector3.up, _myTransform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float hitdist = 0.0f;
            if (playerPlane.Raycast(ray, out hitdist))
            {
                //_targetPoint = ray.GetPoint(hitdist);
                _destinationPosition = ray.GetPoint(hitdist);
            }
        }
    }

    internal Quaternion GetDestination()// ausrichtung des mobs
    {
        var dest = Quaternion.Slerp(_myTransform.rotation, Quaternion.LookRotation(_destinationPosition - _myTransform.position), TurnSpeed * Time.deltaTime);
        dest.x = 0;
        dest.z = 0;
        return dest;
    }
}
