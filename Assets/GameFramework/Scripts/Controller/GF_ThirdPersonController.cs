﻿using GameFramework;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GF_Player))]
[RequireComponent(typeof(GF_ThirdPersonCamController))]

public class GF_ThirdPersonController : GF_InputManager
{
    public CharacterController CharController;

    public GF_Player GF_Player;
    // Use this for initialization

    void Awake()
    {
        CharController = GetComponent<CharacterController>();
        GF_Player = GetComponent<GF_Player>();
    }

    // Update is called once per frame
	void Update () 
    {
        if (GF_Player.controller.isGrounded)
        {
            if (GF_Player.UnitAttribute.CanMove)
            {

                GF_Player.moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical") * GF_Player.UnitAttribute.Speed);
                GF_Player.moveDirection = GF_Player.MyTransform.TransformDirection( GF_Player.moveDirection);
                GF_Player.moveDirection *= GF_Player.UnitAttribute.Speed;

                GF_Player.MyTransform.Rotate(0, Input.GetAxis("Horizontal"), 0);

                if (GF_Player.UnitAttribute.CanJump && Input.GetButton("Jump"))
                {
                    Debug.Log("JUMP");
                     GF_Player.moveDirection.y = GF_Player.UnitAttribute.JumpSpeed;
                }
            }
           
        }
        else
        {
             GF_Player.moveDirection.y -= GF_Player.UnitAttribute.GravityMultiplier * GF_Gamecore.Gravity * Time.deltaTime;
        }
       
        
        if (GF_Player.UnitAttribute.CanMove)
            GF_Player.controller.Move( GF_Player.moveDirection * Time.deltaTime);


	    if (Input.GetAxis("Vertical") > 0)
	    {
          
            if (Input.GetKey(KeyCode.LeftShift))
            {
                GF_Player.UnitAttribute.Speed = GF_Player.UnitAttribute.RunSpeed;
                GF_AnimationManager.Play(GF_Player, "run");
            }
            else
            {
                GF_AnimationManager.Play(GF_Player, "walk");

                GF_Player.UnitAttribute.Speed = GF_Player.UnitAttribute.WalkSpeed;
            }
	    }
	    else if(Input.GetAxis("Vertical") < 0)
        {
            GF_AnimationManager.Play(GF_Player, "backward");
            GF_Player.UnitAttribute.Speed = GF_Player.UnitAttribute.BackSpeed;
        }
	    else
	    {
            GF_AnimationManager.Play(GF_Player, "idle");
	    }



    }
}

