


namespace GameFramework
{
    using System.Collections.Generic;
    public class GF_Scoreboard
    {
        public Dictionary<Team, int> TeamScore = new Dictionary<Team, int>();
        public Dictionary<string, int> PlayersScore = new Dictionary<string, int>();

        public void AddScore(string playerName, int score)
        {
            if (PlayersScore.ContainsKey(playerName))
                PlayersScore[playerName] += score;
            else
                PlayersScore.Add(playerName, score);
        }

        public void AddScore(Team team, int score)
        {
            if (TeamScore.ContainsKey(team))
                TeamScore[team] += score;
            else
                TeamScore.Add(team, score);
        }

        public void AddScore(Team team, string playerName, int score)
        {
            if (TeamScore.ContainsKey(team))
                TeamScore[team] += score;
            else
                TeamScore.Add(team, score);

            if (PlayersScore.ContainsKey(playerName))
                PlayersScore[playerName] += score;
            else
                PlayersScore.Add(playerName, score);
        }
    }
}
