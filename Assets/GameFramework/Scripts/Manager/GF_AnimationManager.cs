using UnityEngine;
using System.Collections;

namespace GameFramework
{
    public class GF_AnimationManager : MonoBehaviour
    {
        public static void Play(GF_Unit unit, string animationName)
        {
            unit.MyTransform.animation.CrossFade(animationName, 0.15f);
        }
    }
}